use std::fs;
use std::time::{Duration, Instant};

fn import_boards(path: &str) -> String {
    fs::read_to_string(path).expect("Unable to read file")
}

pub fn check_row(board: &Vec<Vec<i32>>, row: usize, val: i32) -> bool {
    //! Check that the current row does not contain 'val'
    for i in 0..9 {
        if val == board[row][i] { return false };
    }
    true
}

pub fn check_column(board: &Vec<Vec<i32>>, column: usize, val: i32) -> bool {
    //! Check that the current column does not contain 'val'
    for i in 0..9 {
        if val == board[i][column] { return false };
    }
    true
}

pub fn check_block(board: &Vec<Vec<i32>>, row: i32, column: i32, val: i32) -> bool {
    //! Check that the block does not contain the value 'val'
    // Determine starting value of block
    // i.e. 2/3 = 0
    //      5/3 = 1
    //      8/3 = 2
    let row = (row / 3) * 3;
    let column = (column / 3) * 3;
    for r in 0..3 {
        for c in 0..3 {
            if board[(r+row) as usize][(c+column) as usize] == val { return false }
        }
    }
    true
}

pub fn pretty_print_board(board: &Vec<Vec<i32>>) {
    //! Prints the board from the board vector
    let mut pretty_board: String = "".to_string();
    let mut counter = 1;
    for row in 0..9 {
        for column in 0..9 {
            pretty_board.push_str(&board[row][column].to_string());
            pretty_board.push(' ');
            if counter == 81 {
                break
            }
            else if counter % 27 == 0 {
                pretty_board.push_str("\n------+-------+------\n");
            }
            else if counter % 9 == 0 {
                pretty_board.push('\n');
            }
            else if counter % 3 == 0 {
                pretty_board.push_str("| ");
            }
            counter += 1;
        }
    }
    println!("{}", pretty_board)
}

fn solve(board: &mut Vec<Vec<i32>>) {
    //! While the board is not solved solve it
    for row in 0..9 {
        for col in 0..9 {
            let mut possible_value: i32 = 0;
            if board[row as usize][col as usize] != 0 { continue }
            for val in 0..10 {
                if check_row(&board, row, val) &&
                    check_column(&board, col as usize, val) &&
                    check_block(&board, row as i32, col as i32, val) {
                    if possible_value > 0 {
                        possible_value = 0;
                        break
                    }
                    possible_value = val;
                }
            }
            board[row as usize][col as usize] = possible_value;
        }
    }
}

pub fn eliminate(board: &mut Vec<Vec<i32>>, row: usize, col: usize){
    //! Eliminate possible values from a block
    //! If only one value remains, place it in that position
    //! Run through rows, columns and blocks
    let mut possible_values: String = "123456789".to_string();
    // Eliminate from row, column, block
    // Find block starting point
    let block_row = (row / 3) * 3;
    let block_col = (col / 3) * 3;
    let mut row_counter = 0;
    let mut col_counter = 0;
    for i in 0..9 {
        // Eliminate from row
        possible_values = possible_values.replace(board[row][i as usize]
                                                      .to_string()
                                                      .chars()
                                                      .nth(0)
                                                      .unwrap(), "");
        // Eliminate from column
        possible_values = possible_values.replace(board[i as usize][col]
                                                      .to_string()
                                                      .chars()
                                                      .nth(0)
                                                      .unwrap(), "");
        // Eliminate from block
        possible_values = possible_values.replace(board[block_row + row_counter][block_col + col_counter]
                                                      .to_string()
                                                      .chars()
                                                      .nth(0)
                                                      .unwrap(), "");
        // Iterate through the block
        if row_counter == 2 {
            row_counter = 0;
            col_counter += 1;
        }
        else { row_counter += 1 };
    }
    // Replace position in board with correct value
    if possible_values.len() == 1 {
        board[row][col] = possible_values.parse::<i32>().unwrap();
    }
}

fn main() {
    let values = import_boards("src/puzzles/easy50.txt").replace("\n", "");
    let mut values_vector: Vec<&str> = values.split("========").collect();
    let mut solved_count = 0;
    let now = Instant::now();
    for item in &values_vector {
        solved_count += 1;
        let mut board: Vec<Vec<i32>> = vec![vec![]; 9];
        let mut row_number = 0;
        let mut counter = 0;
        for c in item.chars() {
            board[row_number].push(c.to_string().parse::<i32>().unwrap());
            counter += 1;
            if counter % 9 == 0 { row_number += 1 };
        }
        //Solve sudoku
        let mut not_solved = true;
        let mut counter = 0;
        let mut test_position = 0;
        let mut test_value = 1;
        let mut master_board: Vec<Vec<i32>> = vec![vec![0; 9]; 9];
        while not_solved {
            not_solved = false;
            if counter > 20 {
                // Start Guessing values
                // make a copy of the board at this point
                if test_value == 1 {
                    master_board = board.clone();
                } else if test_value == 10 {
                    test_value = 1;
                    test_position += 1;
                    if test_position == 9 {
                        solved_count -= 1;
                        break
                    }
                }
                board = master_board.clone();
                // Iterate through 1st row if not solved
                board[0][test_position] = test_value;
                counter = 0;
                test_value += 1;
            };
            counter += 1;
            for row in 0..9 {
                for col in 0..9 {
                    if board[row][col] == 0 {
                        not_solved = true;
                        eliminate(&mut board, row, col);
                    };
                }
            }
        }
        println!("{}", solved_count);
    }
    println!("Script took {} seconds to run", now.elapsed().as_secs());
    println!("{}/{} solved, that's {}%", solved_count, 50, 100.0 * (solved_count as f64/50.0));
}
