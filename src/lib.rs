mod main;


#[cfg(test)]
mod tests {
    use crate::main::check_row;
    use crate::main::check_column;
    use crate::main::check_block;
    use crate::main::pretty_print_board;
    use crate::main::eliminate;

    #[test]
    fn test_check_row() {
        let mut board: Vec<Vec<i32>> = vec![vec![0; 10]; 10];
        board[3][4] = 2;
        assert_eq!(true, check_row(&board,2, 2));
        assert_eq!(false, check_row(&board, 3, 2));
    }
    #[test]
    fn test_check_column() {
        let mut board: Vec<Vec<i32>> = vec![vec![0; 10]; 10];
        board[3][4] = 4;
        assert_eq!(true, check_column(&board,6, 4));
        assert_eq!(false, check_column(&board, 4, 4));
    }
    #[test]
    fn test_check_block() {
        //! Check some edge cases.
        //! Looking for 9 tests
        //! As per this diagram
        //! X 5 3 | 9 X 0 | 6 7 X
        //! 9 2 7 | 3 2 5 | 8 3 1
        //! 0 0 0 | 8 7 6 | 4 2 3
        //! ------+-------+------
        //! 3 4 8 | 1 5 2 | 4 6 7
        //! X 1 3 | 4 X 0 | 0 5 X
        //! 5 0 6 | 7 9 3 | 2 1 4
        //! ------+-------+------
        //! 2 3 2 | 6 8 9 | 5 4 0
        //! 8 6 4 | 0 0 3 | 1 0 9
        //! X 7 5 | 0 X 4 | 3 8 X

        let mut board: Vec<Vec<i32>> = vec![vec![0; 10]; 10];
        let mut counter = 1;
        let mut row: usize = 0;
        let mut col: i32 = 0;
        while counter < 10 {
            board[row][col as usize] = counter;
            if col == 8 {
                row += 4;
                col = -4
            }
            col += 4;
            counter += 1;
        }
        // Run tests
        row = 0;
        col = 0;
        let mut test_counter = 1;
        while test_counter < 10 {
            assert_eq!(false, check_block(&board, row as i32, col, test_counter));
            assert_eq!(true, check_block(&board, row as i32, col, test_counter + 1));
            if col == 8 {
                row += 4;
                col = -4
            }
            col += 4;
            test_counter += 1;
        }
    }
    #[test]
    fn sanity_test_check_block() {
        //! Simple test that's easier to comprehend
        let mut board: Vec<Vec<i32>> = vec![vec![0;10]; 10];
        // Put a digit in the middle block
        board[4][4] = 5;
        // Return true, no 5 in these blocks
        assert_eq!(true, check_block(&board, 0, 0, 5));
        assert_eq!(true, check_block(&board, 1, 1, 5));
        assert_eq!(true, check_block(&board,2,2,5));
        assert_eq!(true, check_block(&board, 6,6,5));
        assert_eq!(true, check_block(&board, 7, 7, 5));
        assert_eq!(true, check_block(&board, 8, 8, 5));
        //Return false, all in the middle block with the 5
        assert_eq!(false, check_block(&board, 3, 3, 5));
        assert_eq!(false, check_block(&board, 4, 4, 5));
        assert_eq!(false, check_block(&board, 5,5, 5));
    }

    #[test]
    fn test_eliminate_row() {
        //! Test that having one missing value in a row solves the row.
        //! ...
        //! 1 2 3 | 4 5 6 | 7 8 0
        //! ...
        //! ->
        //! ...
        //! 1 2 3 | 4 5 6 | 7 8 9
        //! ...
        let mut b: Vec<Vec<i32>> = vec![vec![0; 10]; 10];
        for x in 0..8{
            b[2][x] = (x as i32) + 1;
        }
        // We know the value is in the block [2][8]
        assert_eq!(b[2][8], 0);
        eliminate(&mut b, 2, 8);
        assert_eq!(b[2][8], 9);
    }

    #[test]
    fn test_eliminate_column() {
        //! Test that having one missing value in a column solves the column.
        //! ...
        //! Similar layout to above, transposed
        //! ...
        let mut b: Vec<Vec<i32>> = vec![vec![0; 10]; 10];
        for x in 0..8{
            b[x][5] = (x as i32) + 1;
        }
        // We know the value is in the block [2][8]
        assert_eq!(b[8][5], 0);
        eliminate(&mut b, 8, 5);
        assert_eq!(b[8][5], 9);
    }

    #[test]
    fn test_eliminate_row_and_column() {
        //! Test that having one missing value in a column that is in the row solves the column.
        //! ...
        //! For example the row has all the values except for 3 to determine it's a 5
        //! 0 0 0 | 0 0 1 | 0 0 0
        //! 0 0 0 | 0 0 2 | 0 0 0
        //! 0 0 0 | 0 0 0 | 0 0 0
        //! ------+-------+------
        //! 0 0 0 | 0 0 4 | 0 0 0
        //! 0 0 3 | 0 0 0 | 0 0 0
        //! 0 0 0 | 0 0 6 | 0 0 0
        //! ------+-------+------
        //! 0 0 0 | 0 0 7 | 0 0 0
        //! 0 0 0 | 0 0 8 | 0 0 0
        //! 0 0 0 | 0 0 9 | 0 0 0
        //!
        let mut b: Vec<Vec<i32>> = vec![vec![0; 10]; 10];
        for x in 0..9 {
            if x == 4 || x == 2 { continue };
            b[x][5] = (x as i32) + 1;
        }
        // Add in 3 to block 4, 2
        b[4][2] = 3;
        // We know the value is in the block [2][8]
        assert_eq!(b[4][5], 0);
        eliminate(&mut b, 4, 5);
        assert_eq!(b[4][5], 5);
    }

    #[test]
    fn test_eliminate_in_block() {
        //! Test that a block containing all values but 1 solves the missing position
        //! 0 0 0 | 0 0 0 | 0 0 0
        //! 0 0 0 | 0 0 0 | 0 0 0
        //! 0 0 0 | 0 0 0 | 0 0 0
        //! ------+-------+------
        //! 0 0 0 | 1 2 3 | 0 0 0
        //! 0 0 0 | 4 0 6 | 0 0 0
        //! 0 0 0 | 7 8 9 | 0 0 0
        //! ------+-------+------
        //! 0 0 0 | 0 0 0 | 0 0 0
        //! 0 0 0 | 0 0 0 | 0 0 0
        //! 0 0 0 | 0 0 0 | 0 0 0
        //! ->
        //! ...
        //! ------+-------+------
        //! 0 0 0 | 1 2 3 | 0 0 0
        //! 0 0 0 | 4 5 6 | 0 0 0
        //! 0 0 0 | 7 8 9 | 0 0 0
        //! ------+-------+------
        //! ...
        let mut b: Vec<Vec<i32>> = vec![vec![0; 10]; 10];
        b[3][3] = 1;
        b[3][4] = 2;
        b[3][5] = 3;
        b[4][3] = 4;
        b[4][5] = 6;
        b[5][3] = 7;
        b[5][4] = 8;
        b[5][5] = 9;
        assert_eq!(b[4][4], 0);
        eliminate(&mut b, 4, 4);
        assert_eq!(b[4][4], 5);
    }
}